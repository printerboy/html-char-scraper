import requests
import urllib.request
import time
import re
import base64
import csv
from bs4 import BeautifulSoup

url = 'https://www.rarewhisky101.com/indices/market-performance-indices/rw-apex-indices'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
out = soup.find_all('script')
result = re.findall('data: \\[(.*)\\]', str(out))

searchedLabels = re.search('labels: \\[(.*)\\]', str(out))
rwapex100 = result[0].split(',')
rwapex250 = result[1].split(',')
rwapex1000 = result[2].split(',')

labelsList = searchedLabels.group(1).split(',')

dates = []
for r in labelsList:
    o = re.search('"(.*)"', r)
    if o is not None:
        for res in o.groups():
            s = (base64.b64decode(res))
            dates.append(s.decode('utf-8'))

apex100 = []
for r in rwapex100:
    o = re.search('"(.*)"', r)
    if o is not None:
        for res in o.groups():
            s = (base64.b64decode(res))
            apex100.append(s.decode('utf-8'))

apex250 = []
for r in rwapex250:
    o = re.search('"(.*)"', r)
    if o is not None:
        for res in o.groups():
            s = (base64.b64decode(res))
            apex250.append(s.decode('utf-8'))

apex1000 = []
for r in rwapex1000:
    o = re.search('"(.*)"', r)
    if o is not None:
        for res in o.groups():
            s = (base64.b64decode(res))
            apex1000.append(s.decode('utf-8'))

with open('whiskeychart.csv', 'w', newline='') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    data = list(zip(dates, apex100, apex250, apex1000))
    wr.writerow(list(['Datum', 'Rw Apex 100', 'Rw Apex 250', 'Rw Apex 1000']))
    for row in data:
        row = list(row)
        if row[1] and row[2] and row[3] != 'NaN':
            wr.writerow(row)
